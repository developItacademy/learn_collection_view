//
//  ApplicationData.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 8/15/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import Foundation

//struct ApplicationData {
//    var categories: [String]
//    var items: [[String]]
//    var itemsData: [String: [String]]
//
//    init() {
//        categories = ["Level1", "Level2", "Level3"]
//        items = [["STU1010", "STU1011"], ["STU1001", "STU1003", "STU1005", "STU1006", "STU1007", "STU1008", "STU1012"], ["STU1002", "STU1004", "STU1009"]]
//        itemsData = ["STU1001": ["Steve Jobs", "SHOWLEVEL2", "STU1001", "STU1002"],
//                     "STU1002": ["HTML5 for Masterminds", "SHOWLEVEL3", "STU1003"],
//                     "STU1003": ["The Road Ahead", "SHOWLEVEL2", "STU1004"],
//                     "STU1004": ["The C Programming Language", "SHOWLEVEL3", "STU1005"],
//                     "STU1005": ["Being Digital", "SHOWLEVEL2", "STU1006"],
//                     "STU1006": ["Only the Paranoid Survive", "SHOWLEVEL2", "STU1007"],
//                     "STU1007": ["Accidental Empires", "SHOWLEVEL2", "STU1008"],
//                     "STU1008": ["Bobby Fischer Teaches Chess", "SHOWLEVEL2", "STU1009"],
//                     "STU1009": ["New Guide to Science", "SHOWLEVEL3", "STU1010"],
//                     "STU1010": ["Christine", "SHOWLEVEL1", "STU1011"],
//                     "STU1011": ["IT", "SHOWLEVEL1", "STU1012"],
//                     "STU1012": ["Ending Aging", "SHOWLEVEL2", "STU1001"]
//        ]
//    }
//}

struct ApplicationData {
    var categories: [String: String]
    var items: [String]
    var itemsData: [String: [String]]
    
    init() {
        categories = ["17": "Morah Ilana Class", "18": "Morah Chaya Class" , "19": "Morah Chummie Class" , "20": "Morah Shaindy Class" , "21": "Morah Gitty Class", "22" : "Morah Chaya Raizy Class"]
      
        items = ["STU1001", "STU1002", "STU1003", "STU1004", "STU1005", "STU1006", "STU1007", "STU1008", "STU1009",
                 "STU1010", "STU1011", "STU1012", "STU1013", "STU1014", "STU1015", "STU1016", "STU1017", "STU1018" , "STU1019", "STU1020"]
        itemsData = ["STU1001": ["Steve Jobs", "SHOWLEVEL2", "STU1001", "STU1002"],
                     "STU1002": ["HTML5 for Masterminds", "SHOWLEVEL3", "STU1003"],
                     "STU1003": ["The Road Ahead", "SHOWLEVEL2", "STU1004"],
                     "STU1004": ["The C Programming Language", "SHOWLEVEL3", "STU1005"],
                     "STU1005": ["Being Digital", "SHOWLEVEL2", "STU1006"],
                     "STU1006": ["Only the Paranoid Survive", "SHOWLEVEL2", "STU1007"],
                     "STU1007": ["Accidental Empires", "SHOWLEVEL2", "STU1008"],
                     "STU1008": ["Bobby Fischer Teaches Chess", "SHOWLEVEL2", "STU1009"],
                     "STU1009": ["New Guide to Science", "SHOWLEVEL3", "STU1010"],
                     "STU1010": ["Christine", "SHOWLEVEL1", "STU1011"],
                     "STU1011": ["IT", "SHOWLEVEL1", "STU1012"],
                     "STU1012": ["Ending Aging", "SHOWLEVEL2", "STU1001"],
                    "STU1013": ["The Road Ahead", "SHOWLEVEL2", "STU1004"],
                    "STU1014": ["The C Programming Language", "SHOWLEVEL3", "STU1005"],
                    "STU1015": ["Being Digital", "SHOWLEVEL2", "STU1006"],
                    "STU1016": ["Only the Paranoid Survive", "SHOWLEVEL2", "STU1007"],
                    "STU1017": ["Accidental Empires", "SHOWLEVEL2", "STU1008"],
                    "STU1018": ["Bobby Fischer Teaches Chess", "SHOWLEVEL2", "STU1009"],
                    "STU1019": ["New Guide to Science", "SHOWLEVEL3", "STU1010"],
                    "STU1020": ["Christine", "SHOWLEVEL1", "STU1011"]
        ]
    }
}
var AppData = ApplicationData()
