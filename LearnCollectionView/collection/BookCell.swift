//
//  BookCell.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 8/15/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import UIKit

class BookCell: UICollectionViewCell {
    
    @IBOutlet weak var bookCover: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //selectedBackgroundView = UIView()
        //selectedBackgroundView?.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
        bookCover.layer.cornerRadius = 60
        bookCover.layer.masksToBounds = true
    }
    
}
